﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MWApp.Models
{
    public class MaestroModel
    {
        public DepartamentoModel departamento { get; set; }
        public List<EmpleadoModel> empleados { get; set; }
    }
}