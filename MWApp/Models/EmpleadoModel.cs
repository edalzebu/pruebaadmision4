﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MWApp.Models
{
    public class EmpleadoModel
    {
        public int id { get; set; }
        
        [Required]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Required]
        [StringLength(1)]
        [Display(Name = "Sexo")]
        public string sexo { get; set; }

        [Required]
        [StringLength(250)]
        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "F. Nacimiento")]
        public DateTime fecha_nacimiento { get; set; }

        public int id_departamento { get; set; }
    }
}