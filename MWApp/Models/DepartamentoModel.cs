﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Web;

namespace MWApp.Models
{
    public class DepartamentoModel
    {
        public int id { get; set; }
        
        [Required]
        [Display(Name="Nombre")]
        public string nombre { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Referencia")]
        public string referencia { get; set; }

        [Required]
        [StringLength(250)]
        [Display(Name = "Descripción")]
        public string descripcion { get; set; }
    }
}