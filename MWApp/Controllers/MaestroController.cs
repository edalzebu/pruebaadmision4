﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MWApp.MaestroServiceReference;
using MWApp.Models;

namespace MWApp.Controllers
{
    public class MaestroController : Controller
    {
        // GET: Maestro
        [HttpGet]
        public ActionResult MaestroView()
        {
            var maestroModel = new MaestroModel();
            maestroModel.departamento = new DepartamentoModel();
            maestroModel.empleados = new List<EmpleadoModel>();
            maestroModel.empleados.Add(new EmpleadoModel());
            return View(maestroModel);
        }

        [HttpPost]
        public ActionResult MaestroView(MaestroModel model)
        {
            var service = new MaestroServiceReference.MaestroClient();
            var departamento = new MaestroServiceReference.Departamento();
            var empleados = new MaestroServiceReference.Empleado[model.empleados.Count];

            departamento.nombre = model.departamento.nombre;
            departamento.descripcion = model.departamento.descripcion;
            departamento.referencia = model.departamento.referencia;

            for (var i = 0; i < model.empleados.Count; i++)
            {
                empleados[i] = new MaestroServiceReference.Empleado();
                empleados[i].nombre = model.empleados[i].nombre;
                empleados[i].descripcion = model.empleados[i].descripcion;
                empleados[i].sexo = model.empleados[i].sexo;
                empleados[i].fecha_nacimiento = model.empleados[i].fecha_nacimiento;
            }

            service.CreateMaestro(departamento,empleados);

            service.Close();

            return this.MaestroView();
        }

        [HttpGet]
        public ActionResult AgregarEmpleado(MaestroModel model)
        {
            model.empleados.Add(new EmpleadoModel());
            return this.MaestroView();
        }
    }
}