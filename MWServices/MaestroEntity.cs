namespace MWServices
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MaestroEntity : DbContext
    {
        public MaestroEntity()
            : base("name=Maestro")
        {
        }

        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<Empleado> Empleados { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Departamento>()
                .Property(e => e.referencia)
                .IsFixedLength();

            modelBuilder.Entity<Departamento>()
                .Property(e => e.descripcion)
                .IsFixedLength();

            modelBuilder.Entity<Departamento>()
                .HasMany(e => e.Empleados)
                .WithRequired(e => e.Departamento)
                .HasForeignKey(e => e.id_departamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empleado>()
                .Property(e => e.sexo)
                .IsFixedLength();

            modelBuilder.Entity<Empleado>()
                .Property(e => e.descripcion)
                .IsFixedLength();
        }
    }
}
