﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MWServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Maestro" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Maestro.svc or Maestro.svc.cs at the Solution Explorer and start debugging.
    public class Maestro : IMaestro
    {
        public void CreateMaestro(Departamento departamento, Empleado[] empleados)
        {
            var db = new MaestroEntity();
            db.Departamentos.Add(departamento);
            db.Entry(departamento).State = EntityState.Added;
            db.SaveChanges();

            foreach (var empleado in empleados)
            {
                empleado.id_departamento = departamento.id;
                db.Empleados.Add(empleado);
                db.Entry(empleado).State = EntityState.Added;
            }

            db.SaveChanges();
        }
    }
}
