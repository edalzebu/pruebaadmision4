﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MWServices
{
    [ServiceContract]
    public interface IMaestro
    {
        [OperationContract]
        void CreateMaestro(Departamento departamento, Empleado[] empleados);
    }
}
